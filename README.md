# WildFyre Multiplatform Library

Official library created by WildFyre to enable easy access to the WildFyre API from multiple technologies, powered by Kotlin/Multiplatform.

Official links:
- [GitLab repository](https://gitlab.com/wildfyre/lib)
- [Phabricator mirror](https://phabricator.wildfyre.net/source/libwf-java/) (was previously the official repository)
- [GitHub mirror](https://github.com/wildfyre-app/jvm)
- [Bugtracker](https://phabricator.wildfyre.net/tag/libwf-java/)
- [Website](https://wildfyre.net)
