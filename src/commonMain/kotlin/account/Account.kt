/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.account

import io.ktor.http.HttpMethod
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiAccount
import net.wildfyre.lib.api.ApiMutableAvatar
import net.wildfyre.lib.api.ApiMutableBio
import net.wildfyre.lib.users.User

/**
 * The user's account.
 *
 * Can be used to edit the user's information.
 */
class Account internal constructor(private val wildfyre: WildFyre, private var api: ApiAccount) {

    /**
     * The user's [id].
     */
    val id: Int
        get() = api.id

    /**
     * The user's username. Cannot be edited.
     */
    val username: String
        get() = api.username

    /**
     * The user's email address.
     */
    val email: String?
        get() = if (api.email.isBlank()) null else api.email

    /**
     * The [User] object owned by this account.
     */
    suspend fun me() = wildfyre.users.get(id)

    /**
     * Requests the API to change the [avatar] of the user.
     *
     * Note that [avatar] should not be empty. You can set it to `null` to remove the user's avatar.
     */
    suspend fun setAvatar(avatar: String?) {
        require(avatar != "") { "The avatar cannot be empty" }

        wildfyre.api.request<String>(HttpMethod.Patch, "/users/", json = ApiMutableAvatar(avatar))
        wildfyre.me().flagAsOutdated()
    }

    /**
     * Requests the API to change the [bio] of the user.
     *
     * To remove the bio, input an empty string.
     *
     * Note that no user object will be modified. To get an updated object, you will
     * have to query the object again.
     */
    suspend fun setBio(bio: String) {
        wildfyre.api.request<String>(HttpMethod.Patch, "/users/", json = ApiMutableBio(bio))
        wildfyre.me().flagAsOutdated()
    }

    /**
     * Refreshes this account.
     *
     * This is useful in particular if the user's [email] address has changed.
     * Note that [id] and [username] cannot be edited, and therefore cannot change.
     *
     * The modification of this object for new state is atomic.
     */
    suspend fun refresh() {
        val res = wildfyre.api.request<ApiAccount>(HttpMethod.Get, "/account/")

        check(id == res.id) {
            "Just refreshed the account... " + "Previously had ID $id but I now have ID ${res.id}? That shouldn't happen."
        }

        api = res
    }

}
