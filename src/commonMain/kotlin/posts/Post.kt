/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.posts

import io.ktor.http.HttpMethod
import io.ktor.http.Url
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiPost
import net.wildfyre.lib.api.ApiSubscription
import net.wildfyre.lib.users.User

/**
 * Allows to access posts.
 *
 * To get an instance of this class, see [PostCache] (in particular, [PostCache.nextPosts] and [PostCache.myPosts]).
 */
class Post internal constructor(
    private val wildfyre: WildFyre, override val cache: PostCache, private val api: ApiPost
) : PostData {

    override val id: Int
        get() = api.id

    /**
     * Has the authenticated user subscribed to this post?
     *
     * When subscribed, the user will receive a notification when a new comment is posted.
     *
     * To subscribe to this post, see [subscribe] and [unsubscribe].
     */
    val subscribed: Boolean
        get() = api.subscribed

    /**
     * Subscribes to this post, and flags this object as out-of-date.
     * You can then call [getNewVersion] to get the updated object.
     * This operation will request the server even if the user has already subscribed.
     *
     * Posting a comment will automatically subscribe the user.
     *
     * See [subscribed] and [unsubscribe].
     */
    suspend fun subscribe() {
        handleSubscription(true)
    }

    /**
     * Unsubscribes from this post, and flags the object as out-of-date.
     * You can call [getNewVersion] to get the updated object.
     * This operation will request the server even if the user has not subscribed.
     *
     * Posting a comment will automatically subscribe the user.
     *
     * See [subscribed] and [subscribe].
     */
    suspend fun unsubscribe() {
        handleSubscription(false)
    }

    private suspend fun handleSubscription(subscribe: Boolean) {
        wildfyre.api.request<Unit>(
            HttpMethod.Put, "/areas/${cache.area}/$id/subscribe/", json = ApiSubscription(subscribe)
        )
        cache.flagAsOutdated(id)
    }

    override suspend fun author(): User? = api.author?.let { wildfyre.users.get(it) }

    override val anonymous: Boolean
        get() = api.anonym

    override val text: String
        get() = api.text

    override val mainImage: Url?
        get() = api.image?.let { Url(it) }

    override val secondaryImages: List<Url>
        get() = api.additional_images.map { Url(it) }

    /**
     * The date of creation of this post.
     *
     * Example value: '2020-04-30T12:09:45.373523Z'.
     */
    val created: String
        get() = api.created

    /**
     * Is this post active?
     *
     * A post is active if it can still be spread.
     */
    val active: Boolean
        get() = api.active

    /**
     * The comments that were posted here.
     */
    val comments: List<Comment> by lazy {
        api.comments.map { Comment(wildfyre, it) }
    }

    //TODO in T401: post comments

    override fun toString() = api.toString()

    override suspend fun getNewVersion(forceUpdate: Boolean) = super.getNewVersion(forceUpdate) as Post
}
