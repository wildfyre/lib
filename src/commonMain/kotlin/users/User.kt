/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.users

import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.account.Account
import net.wildfyre.lib.api.ApiUser
import net.wildfyre.lib.utils.Cache
import net.wildfyre.lib.utils.CachedObject

/**
 * Representation of a user.
 */
class User internal constructor(internal val wildfyre: WildFyre, private val user: ApiUser) : CachedObject<Int, User> {

    /**
     * This user's ID. Cannot be negative.
     */
    override val id: Int
        get() = user.user

    /**
     * This user's name. Cannot be blank.
     */
    val username: String
        get() = user.name

    /**
     * This user's avatar, represented as a URL to the correct file.
     *
     * If this user is yourself, you can edit this field. See [isMe] for more information.
     */
    val avatar: String?
        get() = user.avatar

    /**
     * This user's biography. Can be blank.
     *
     * If this user is yourself, you can edit this field. See [isMe] for more information.
     */
    val bio: String
        get() = user.bio

    /**
     * Is this user banned by the system?
     */
    val banned: Boolean
        get() = user.banned

    /**
     * Is this user representing me?
     *
     * If this is `true`, then you can edit some properties of this object.
     * See [account].
     */
    val isMe: Boolean = wildfyre.account.id == id

    /**
     * Gets this user's account, which can be used to edit this user.
     *
     * This property holds `null` if the user is not yourself. You can check this with [isMe] as well.
     *
     * This property is strictly equivalent to [WildFyre.account].
     */
    val account: Account?
        get() = if (isMe) wildfyre.account else null

    override fun toString() = user.toString()

    override fun hashCode(): Int {
        return id
    }

    override val cache: Cache<Int, User>
        get() = wildfyre.users
}
