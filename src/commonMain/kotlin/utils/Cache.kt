/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.utils

/**
 * Allows to cache values, indexed by an ID of type [I].
 */
interface Cache<I, T> {

    /**
     * Queries an object by its [id].
     *
     * This method can suspend until the object is ready.
     */
    suspend fun get(id: I): T

    /**
     * Flag an object as outdated, so it will be downloaded again next time it is
     * requested.
     *
     * If the object is currently being waited upon, this method may not do
     * anything (no need to mark as outdated an object that is currently being
     * downloaded anyway).
     *
     * @param id The ID of the object that should be marked as outdated
     */
    suspend fun flagAsOutdated(id: I)

    /**
     * Forces the cache to accept a [value] (based on its [id]), overrides any previous cached object.
     */
    suspend fun forceAddToCache(id: I, value: T)
}
