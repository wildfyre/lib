/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.utils

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import net.wildfyre.lib.WildFyre

class SemaphoreCache<I, T>(wildfyre: WildFyre, private val worker: suspend (id: I) -> T) : Cache<I, T> {

    private val scope = CoroutineScope(SupervisorJob(wildfyre.config.job))
    private val cache = HashMap<I, Deferred<T>>()
    private val cacheSemaphore = Semaphore(1)

    override suspend fun get(id: I): T {
        val result = cacheSemaphore.withPermit {
            cache.getOrPut(id) { scope.async { worker(id) } }
        }
        return result.await()
    }

    override suspend fun flagAsOutdated(id: I) {
        cacheSemaphore.withPermit {
            cache[id]?.let { request ->
                if (request.isCompleted) {
                    @Suppress("DeferredResultUnused") cache.remove(id)
                } // else: the request is ongoing, no need to mark it as outdated
            } // else: there is no value in the cache, there's nothing to do
        }
    }

    override suspend fun forceAddToCache(id: I, value: T) {
        cacheSemaphore.withPermit {
            cache[id] = CompletableDeferred(value)
        }
    }
}
