/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.http

import io.ktor.client.HttpClient
import io.ktor.client.features.ClientRequestException
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.contentLength
import io.ktor.http.contentType
import kotlinx.serialization.json.Json

private val client = HttpClient {
    install(JsonFeature) {
        serializer = KotlinxSerializer(Json.nonstrict)
    }
}

private suspend inline fun <reified T> request(method: HttpMethod, baseUrl: String, endpoint: String, json: Any? = null, token: String? = null, setup: HttpRequestBuilder.() -> Unit = {}): T {
    val logMessage = StringBuilder("\nREQUEST: ${method.value} $baseUrl$endpoint")

    try {
        return client.request {
            this.method = method
            url("${baseUrl}${endpoint}")

            if (json != null) {
                contentType(ContentType.Application.Json)
                body = json
            }

            if (token != null) {
                logMessage.append(" with token $token")
                header("Authorization", "token $token")
            }

            setup()
            println(logMessage)
        }
    } catch (e: ClientRequestException) {
        val result = e.response.readEverything()

        throw RuntimeException("Problem when contacting the API...\n" + "Summary: ${e.response}\n" + "Server's reply: $result", e)
    }
}

internal suspend fun HttpResponse.readEverything(): String {
    var offset = 0
    val byteArray = ByteArray(contentLength()!!.toInt())

    do {
        val currentRead = content.readAvailable(byteArray, offset, byteArray.size)
        offset += currentRead
    } while (currentRead > 0)

    return byteArray.joinToString("") { it.toChar().toString() }
}

internal data class ApiAccess(val baseUrl: String, val token: String?) {

    suspend inline fun <reified T> request(method: HttpMethod, endpoint: String, json: Any? = null, setup: HttpRequestBuilder.() -> Unit = {}) = request<T>(method, baseUrl, endpoint, json, token, setup)

    companion object {

        fun anonymous(apiUrl: String) = ApiAccess(apiUrl, null)

        fun authenticated(apiUrl: String, token: String) = ApiAccess(apiUrl, token)

    }

}
