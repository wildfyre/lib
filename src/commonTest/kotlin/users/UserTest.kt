/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package users

import TestUtils.runTest
import freshTestingApi
import testingApi
import kotlin.test.*

class UserTest {

    @Test
    fun get_me() = runTest {
        val api = testingApi()

        with(api.users.get(api.account.id)) {
            assertEquals(2, id)
            assertEquals("user", username)
            assertEquals(null, avatar)
            assertEquals("", bio)
            assertEquals(false, banned)
        }
    }

    @Test
    fun get_another_user() = runTest {
        val api = testingApi()

        val user = api.users.get(3)

        with(user) {
            assertEquals(3, id)
            assertEquals("another-user", username)
            assertEquals(null, avatar)
            assertEquals("", bio)
            assertEquals(false, banned)
        }
    }

    @Test
    fun is_me() = runTest {
        val api = testingApi()

        val me = api.me()
        val other = api.users.get(3)

        assertTrue(me.isMe)
        assertFalse(other.isMe)

        assertEquals("user", me.account?.username)
        assertNull(other.account)
    }

    @Test
    fun set_user_info() = runTest {
        val api = freshTestingApi()

        println("Checking that I am the correct person...")
        with(api.me()) {
            assertTrue(username.startsWith("new-user"))
            assertNull(avatar)
            assertTrue(bio.isBlank())
        }

        println("Editing my informations...")
        val newAvatar = "https://somewhere.net/test.png"
        val newBio = "This is my new bio"
        with(api.account) {
            //setAvatar(newAvatar)
            setBio(newBio)
        }
        api.me().flagAsOutdated()

        println("Checking that the new values are correct...")
        with(api.me()) {
            println(this)
            //assertEquals(newAvatar, avatar)
            assertEquals(newBio, bio)
        }
    }
}
