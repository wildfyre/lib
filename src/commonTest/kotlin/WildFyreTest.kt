/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

import TestUtils.runTest
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.WildFyre.Companion.LOCAL_API
import kotlin.random.Random
import kotlin.test.Test

const val test_username = "user"
const val test_password = "password$123"

suspend fun testingApi() = WildFyre.connect(test_username, test_password, LOCAL_API)

suspend fun freshTestingApi() = WildFyre.createNewAccount("new-user-${Random.nextInt()}", "test@example.testing", "password\$123", "the captcha is not necessary in debug mode", LOCAL_API)

class WildFyreTest {

    @Test
    fun connect_with_username_password() = runTest {
        testingApi()
    }

    @Test
    fun connect_with_token() = runTest {
        val api = testingApi()
        val token = api.token

        WildFyre.connect(token, LOCAL_API)
    }

    @Test
    fun create_new_account() = runTest {
        freshTestingApi()
    }

}
