/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package areas

import TestUtils.runTest
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.areas.Area
import testingApi
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

suspend fun sampleArea(api: WildFyre): Area {
    val sampleArea = api.areas.getAll().find { it.id == "sample" }
    assertNotNull(sampleArea)
    return sampleArea
}

class AreaTest {

    @Test
    fun get_all_areas() = runTest {
        val api = testingApi()

        val areas = api.areas.getAll()

        val sampleArea = areas.find { it.id == "sample" }
        val secondArea = areas.find { it.id == "secoundarea" }

        assertNotNull(sampleArea)
        assertNotNull(secondArea)

        assertEquals("Sample", sampleArea.displayName)
        assertEquals("2nd Area", secondArea.displayName)
    }

    @Test
    fun get_reputation() = runTest {
        val sampleArea = sampleArea(testingApi())

        assertEquals(0, sampleArea.reputation())
        assertEquals(4, sampleArea.spread())
    }

}
