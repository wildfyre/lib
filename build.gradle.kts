import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootExtension
import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsRootPlugin

/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

//region Plugins
plugins {
    kotlin("multiplatform") version "1.3.61"
    kotlin("plugin.serialization") version "1.3.61"
}

repositories {
    mavenCentral()
}
//endregion

group = "net.wildfyre"
calculateVersion()

//region Calculate the version
/**
 * Calculates the version by calling 'git describe' to find the latest tag.
 *
 * @param timeout How long (in seconds) should this function be running before
 *                an exception is thrown.
 */
fun calculateVersion(timeout: Long = 60) {
    val command = listOf("git", "describe", "--tags", "--candidates=30", "--dirty=-SNAPSHOT")

    val process = ProcessBuilder(*command.toTypedArray())
        .directory(projectDir)
        .redirectOutput(ProcessBuilder.Redirect.PIPE)
        .start()

    process.waitFor(timeout, TimeUnit.SECONDS)

    val describe = process.inputStream.bufferedReader()
        .readLine()
        ?: error("'git describe' didn't return anything!")

    version = describe.substring(describe.indices)
        .replaceFirst("-\\d+-", "-")

    println("Detected project version: $version")
}
//endregion

//region Check that the WildFyre API is running
tasks.register("checkApiIsRunning", type = Exec::class) {
    group = "Test"
    description = "Checks that the API is running. Currently, only works on Linux-based OS. On Windows machines, " +
        "updates the submodule and trusts the user that the API is running."

    if (org.gradle.internal.os.OperatingSystem.current().isUnix) {
        // Doesn't start the API (the user should start it themselves in another terminal to be able to read its
        // output).
        commandLine("./api.sh", "--norun")
    } else {
        println("Cannot check if the API is running correctly.")
    }
}
//endregion

//region Kotlin Multiplatform
val ktor_version = "1.3.1"
val serialization_version = "0.14.0"

kotlin {
    jvm()
    js {
        browser()
    }

    @Suppress("UNUSED_VARIABLE")
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:$serialization_version")
                implementation("io.ktor:ktor-client-core:$ktor_version")
                implementation("io.ktor:ktor-client-json:$ktor_version")
                implementation("io.ktor:ktor-client-serialization:$ktor_version")
            }
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:$serialization_version")
                implementation("io.ktor:ktor-client-apache:$ktor_version")
                implementation("io.ktor:ktor-client-json-jvm:$ktor_version")
                implementation("io.ktor:ktor-client-serialization-jvm:$ktor_version")
            }
        }

        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:$serialization_version")
                implementation("io.ktor:ktor-client-js:$ktor_version")
                implementation("io.ktor:ktor-client-json-js:$ktor_version")
                implementation("io.ktor:ktor-client-serialization-js:$ktor_version")
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}
//endregion

//region Choose whether Node is downloaded or not
plugins.withType<NodeJsRootPlugin> {
    configure<NodeJsRootExtension> {
        download = !project.hasProperty("noNodeDownload")
    }
}
//endregion

//region File utility functions
operator fun File.get(child: String) = File(this, child)
//endregion
